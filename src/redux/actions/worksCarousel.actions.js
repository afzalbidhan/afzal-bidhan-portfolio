import axios from "axios";
import { WORK_CAROUSEL_DATA_FAIL, WORK_CAROUSEL_DATA_REQUEST, WORK_CAROUSEL_DATA_SUCCESS } from "../actionType";


export const worksCarouselAction = () => async dispatch => {
    try {
        dispatch({ type: WORK_CAROUSEL_DATA_REQUEST });
        const result = await axios.get("https://afzal-bidhan-d007.herokuapp.com/projectvisit_AB007_v1");
        dispatch({
            type: WORK_CAROUSEL_DATA_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: WORK_CAROUSEL_DATA_FAIL,
            payload: error.message,
        });
    };
};