import { SIDEBAR_TOGGLE } from "../actionType";

export const toogleSidebar = () => async dispatch => {

    try {
        dispatch({
            type: SIDEBAR_TOGGLE,
        });

    } catch (error) {
        console.log(error.message);
    };
};