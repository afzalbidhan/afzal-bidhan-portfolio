import axios from "axios";
import { ALL_SOCIALLINKS_SUCCESS, ALL_SOCIALLINKS_REQUEST, SOCIALLINKS_FAIL, SINGLE_SOCIALLINK_SUCCESS, SINGLE_SOCIALLINK_REQUEST, TWITTER_LINK_REQUEST, TWITTER_LINK_SUCCESS, FACEBOOK_LINK_REQUEST, FACEBOOK_LINK_SUCCESS, INSTAGRAM_LINK_SUCCESS, INSTAGRAM_LINK_REQUEST, LINKDIN_LINK_REQUEST, LINKDIN_LINK_SUCCESS, GITHUB_LINK_REQUEST, GITHUB_LINK_SUCCESS } from "../actionType";


// get all Social Links
export const allSocialLinks = () => async dispatch => {
    try {
        dispatch({ type: ALL_SOCIALLINKS_REQUEST });
        const result = await axios.get("https://afzal-bidhan-d007.herokuapp.com/socialLinksVisit_AB065_v1");
        dispatch({
            type: ALL_SOCIALLINKS_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        // alert(error.message);
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};

// get single Social Link
export const singleSocialLink = (id) => async dispatch => {
    try {
        dispatch({ type: SINGLE_SOCIALLINK_REQUEST });
        const result = await axios.get(`https://afzal-bidhan-d007.herokuapp.com/singleSocialLink_AB782_v3/${id}`);
        dispatch({
            type: SINGLE_SOCIALLINK_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        // alert(error.message);
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};

// get twitter Link
export const twitterLink = () => async dispatch => {
    try {
        dispatch({ type: TWITTER_LINK_REQUEST });
        const result = await axios.get(`https://afzal-bidhan-d007.herokuapp.com/singleSocialLink_AB782_v3/1111`);
        dispatch({
            type: TWITTER_LINK_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};
// get facebook Link
export const facebookLink = () => async dispatch => {
    try {
        dispatch({ type: FACEBOOK_LINK_REQUEST });
        const result = await axios.get(`https://afzal-bidhan-d007.herokuapp.com/singleSocialLink_AB782_v3/2222`);
        dispatch({
            type: FACEBOOK_LINK_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};
// get instagram Link
export const instagramLink = () => async dispatch => {
    try {
        dispatch({ type: INSTAGRAM_LINK_REQUEST });
        const result = await axios.get(`https://afzal-bidhan-d007.herokuapp.com/singleSocialLink_AB782_v3/3333`);
        dispatch({
            type: INSTAGRAM_LINK_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};
// get linkdin Link
export const linkdinLink = () => async dispatch => {
    try {
        dispatch({ type: LINKDIN_LINK_REQUEST });
        const result = await axios.get(`https://afzal-bidhan-d007.herokuapp.com/singleSocialLink_AB782_v3/4444`);
        dispatch({
            type: LINKDIN_LINK_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};
// get github Link
export const githubLink = () => async dispatch => {
    try {
        dispatch({ type: GITHUB_LINK_REQUEST });
        const result = await axios.get(`https://afzal-bidhan-d007.herokuapp.com/singleSocialLink_AB782_v3/5555`);
        dispatch({
            type: GITHUB_LINK_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        // alert(error.message)
        console.log(error.message);
        dispatch({
            type: SOCIALLINKS_FAIL,
            payload: error.message,
        });
    };
};
