import { FILTER_WORKS_FAIL, FILTER_WORK_CATEGORY_NAME, FILTER_WORKS_SUCCESS } from "../actionType";

export const filterWorksAction = (data, value) => async dispatch => {
    try {
        dispatch({
            type: FILTER_WORK_CATEGORY_NAME,
            payload: value,
        });
        dispatch({
            type: FILTER_WORKS_SUCCESS,
            payload: data.filter((curElem) => {
                return curElem.category === value;
            })
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: FILTER_WORKS_FAIL,
            payload: error.message,
        });
    };
};
