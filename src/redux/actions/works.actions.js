import axios from "axios";
import { WORKS_FAIL, WORKS_REQUEST, WORKS_SUCCESS, WORK_PAGE_NUMBER_FAIL, WORK_PAGE_NUMBER_SUCCESS } from "../actionType";


export const getWorks = () => async dispatch => {
    try {
        dispatch({ type: WORKS_REQUEST });
        const result = await axios.get("https://afzal-bidhan-d007.herokuapp.com/projectvisit_AB007_v1");
        dispatch({
            type: WORKS_SUCCESS,
            payload: result.data,
        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: WORKS_FAIL,
            payload: error.message,
        });
    };
};

export const workPageAction = (selected) => async dispatch => {
    try {
        dispatch({
            type: WORK_PAGE_NUMBER_SUCCESS,
            payload: selected,

        });
    } catch (error) {
        console.log(error.message);
        dispatch({
            type: WORK_PAGE_NUMBER_FAIL,
            payload: error.message,
        });
    };
};
