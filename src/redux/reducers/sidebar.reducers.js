import { SIDEBAR_TOGGLE } from "../actionType";

const initialState = {
    sidebarToggle: false,
};

export const sidebarToogleReducer = (state = initialState, action) => {

    const { type } = action;

    switch (type) {

        case SIDEBAR_TOGGLE:
            return {
                ...state,
                sidebarToggle: !state.sidebarToggle,
            };

        default:
            return state;
    };

};