import { ALL_SOCIALLINKS_SUCCESS, FACEBOOK_LINK_SUCCESS, GITHUB_LINK_SUCCESS, INSTAGRAM_LINK_SUCCESS, LINKDIN_LINK_SUCCESS, SINGLE_SOCIALLINK_SUCCESS, TWITTER_LINK_SUCCESS } from '../actionType';

const initialState = {
    allLinks: [],
    singleLink: null,
    twitter: '',
    facebook: '',
    instagram: '',
    linkdin: '',
    github: ''
};

export const socialLinks = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {
        case ALL_SOCIALLINKS_SUCCESS:
            return {
                ...state,
                allLinks: payload,
            };
        case SINGLE_SOCIALLINK_SUCCESS:
            return {
                ...state,
                singleLink: payload,
            };
        case TWITTER_LINK_SUCCESS:
            return {
                ...state,
                twitter: payload.socialURL,
            };
        case FACEBOOK_LINK_SUCCESS:
            return {
                ...state,
                facebook: payload.socialURL,
            };
        case INSTAGRAM_LINK_SUCCESS:
            return {
                ...state,
                instagram: payload.socialURL,
            };
        case LINKDIN_LINK_SUCCESS:
            return {
                ...state,
                linkdin: payload.socialURL,
            };
        case GITHUB_LINK_SUCCESS:
            return {
                ...state,
                github: payload.socialURL,
            };

        default:
            return state;
    };
};