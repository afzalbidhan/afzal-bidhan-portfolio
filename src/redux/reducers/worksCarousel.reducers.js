import { WORK_CAROUSEL_DATA_REQUEST, WORK_CAROUSEL_DATA_SUCCESS } from "../actionType";

const initialState = {
    worksCarouselData: [],
    CarouselDataLoading: false,
};


export const worksCarouselReducers = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {
        case WORK_CAROUSEL_DATA_SUCCESS:
            return {
                ...state,
                worksCarouselData: payload,
                CarouselDataLoading: true,
            };
        case WORK_CAROUSEL_DATA_REQUEST:
            return {
                ...state,
                CarouselDataLoading: false,
            };
        default:
            return state;
    };


};