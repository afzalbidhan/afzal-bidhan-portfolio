import { FILTER_WORK_CATEGORY_NAME, FILTER_WORKS_SUCCESS } from "../actionType";

const initialState = {
    filterWorksData: [],
    filterWorkName: "",
};

export const filterWorksReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        case FILTER_WORK_CATEGORY_NAME:
            return {
                ...state,
                filterWorkName: payload,
            };
        case FILTER_WORKS_SUCCESS:
            return {
                ...state,
                filterWorksData: payload,
            };

        default:
            return state;
    };

};