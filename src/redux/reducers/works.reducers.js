import { WORKS_REQUEST, WORKS_SUCCESS, WORK_PAGE_NUMBER_SUCCESS } from "../actionType";

const initialState = {
    worksData: [],
    loading: false,
    pageNumber: 0,
};


export const storeWorks = (state = initialState, action) => {

    const { type, payload } = action;


    switch (type) {
        case WORKS_SUCCESS:
            return {
                ...state,
                worksData: payload,
                worksDataCarousel: payload,
                loading: true,
            };
        case WORKS_REQUEST:
            return {
                ...state,
                loading: false,
            };
        case WORK_PAGE_NUMBER_SUCCESS:
            return {
                ...state,
                pageNumber: payload,
            };
        default:
            return state;
    };


};