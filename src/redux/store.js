import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { filterWorksReducer } from "./reducers/filterWork.reducers";
import { sidebarToogleReducer } from "./reducers/sidebar.reducers";
import { socialLinks } from "./reducers/socialLinks.reducers";
import { storeWorks } from "./reducers/works.reducers";
import { worksCarouselReducers } from "./reducers/worksCarousel.reducers";

const rootReducer = combineReducers({
    sideBarToggle: sidebarToogleReducer,
    links: socialLinks,
    works: storeWorks,
    filterWorksReducer: filterWorksReducer,
    worksCarouselReducers: worksCarouselReducers,
});

const store = createStore(
    rootReducer,
    {},
    composeWithDevTools(applyMiddleware(thunk))
);

export default store;