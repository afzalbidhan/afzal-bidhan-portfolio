import html from "../images/skill_logo/html.png";
import css from "../images/skill_logo/css-3.png";
import sass from "../images/skill_logo/sass.png";
import bootstrap from "../images/skill_logo/bootstrap.png";
import jquery from "../images/skill_logo/jquery-10-1175155.png";
import typescript from "../images/skill_logo/typescript.png";
import javascript from "../images/skill_logo/javascript.png";
import redux from "../images/skill_logo/redux-283024.png";
import react from "../images/skill_logo/physics.png";
import next from "../images/skill_logo/next-js.svg";
import node from "../images/skill_logo/node-js-2.png";
import express from "../images/skill_logo/express.png";
import firebase from "../images/skill_logo/google_firebase-2-512.png";
import mongodb from "../images/skill_logo/mongodb.png";

const aboutSkillBar = [
    {
        id: 1,
        name: "Html5",
        img: html,
        color: "#5D6D7E",
        dataProgress: "77",
        duration: 2300,
        basic: "",
    },
    {
        id: 2,
        name: "Css3",
        img: css,
        color: "#D7E12A",
        dataProgress: "68",
        duration: 1800,
        basic: "",
    },
    {
        id: 3,
        name: "Scss",
        img: sass,
        color: "#ff4c60",
        dataProgress: "60",
        duration: 1700,
        basic: "",
    },
    {
        id: 4,
        name: "Bootstrap",
        img: bootstrap,
        color: "#34EAF5",
        dataProgress: "65",
        duration: 1900,
        basic: "",
    },
    {
        id: 5,
        name: "JavaScript",
        img: javascript,
        color: "#f0db4f",
        dataProgress: "70",
        duration: 2000,
        basic: "",
    },
    {
        id: 6,
        name: "Typescript",
        img: typescript,
        color: "#17A589",
        dataProgress: "60",
        duration: 2100,
        basic: "",
    },
    {
        id: 7,
        name: "jquery",
        img: jquery,
        color: "#6C3483",
        dataProgress: "60",
        duration: 1200,
        basic: "",
    },
    {
        id: 8,
        name: "React",
        img: react,
        color: "#0284FF",
        dataProgress: "71",
        duration: 2400,
        basic: "",
    },
    {
        id: 9,
        name: "Redux",
        img: redux,
        color: "#764abc",
        dataProgress: "67",
        duration: 1900,
        basic: "",
    },
    {
        id: 10,
        name: "Next",
        img: next,
        color: "#212F3D",
        dataProgress: "55",
        duration: 2200,
        basic: "",
    },
    {
        id: 11,
        name: "Node",
        img: node,
        color: "#3c873a",
        dataProgress: "40",
        duration: 1400,
        basic: "(Basic)",
    },
    {
        id: 12,
        name: "Express",
        img: express,
        color: "#68a063",
        dataProgress: "37",
        duration: 2600,
        basic: "(Basic)",
    },
    {
        id: 13,
        name: "Firebase",
        img: firebase,
        color: "#FFA611",
        dataProgress: "42",
        duration: 1500,
        basic: "(Basic)",
    },
    {
        id: 14,
        name: "MongoDB",
        img: mongodb,
        color: "#3FA037",
        dataProgress: "35",
        duration: 1900,
        basic: "(Basic)",
    },
];

export default aboutSkillBar;