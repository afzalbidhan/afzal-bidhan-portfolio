import img4 from '../images/carousel_img/1.svg';
import img2 from '../images/carousel_img/3.svg';
import img3 from '../images/carousel_img/5.svg';
import img1 from '../images/carousel_img/6.svg';

const aboutCarousel = [
    { id: 1, img: img1 },
    { id: 2, img: img2 },
    { id: 3, img: img3 },
    { id: 4, img: img4 },
];

export default aboutCarousel;