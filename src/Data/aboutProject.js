
const aboutProject = [
    {
        id: 1,
        name: "Projects completed",
        ionClass: "fab fa-docker",
        num: 25,
        dur: 2500,
    },
    {
        id: 2,
        name: "Cup of coffee",
        ionClass: "fas fa-mug-hot",
        num: 88,
        dur: 3200,
    },
    {
        id: 3,
        name: "Satisfied clients",
        ionClass: "fas fa-users",
        num: 59,
        dur: 2900,
    },
    {
        id: 4,
        name: "Nominees winner",
        ionClass: "fas fa-award",
        num: 15,
        dur: 2200,
    },
];

export default aboutProject;