import img1 from '../images/aboutInfo_img/download.svg';
import img3 from '../images/aboutInfo_img/download3.svg';
import img2 from '../images/aboutInfo_img/downloa1.svg';
import img4 from '../images/aboutInfo_img/download5.svg';

const aboutInfo = [
    {
        id: 1,
        img: img1,
        name: "Atomic",
        description1: "Based on Atomic Design",
        description2: "Methodology.",
    },
    {
        id: 2,
        img: img3,
        name: "Customisation",
        description1: "Multiple customisations",
        description2: "to suit your style",
    },
    {
        id: 3,
        img: img2,
        name: "Responsive",
        description1: "Build fully responsive",
        description2: "tructures easily.",
    },
    {
        id: 4,
        img: img4,
        name: "Theme Setup",
        description1: "Auto updating colors and",
        description2: "Styleguide.",
    },

];

export default aboutInfo;