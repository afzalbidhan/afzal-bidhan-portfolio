import React from 'react';
import './_homeScreen.scss';
import Home from '../../components/Home/Home';
import About from '../../components/About/About';
import Experience from '../../components/Experience/Experience';
import Work from '../../components/Work/Work';
import TopbarMain from '../../components/Topbar/TopbarMain';
import Contact from '../../components/Contact/Contact';
import Footer from './../../components/Footer/Footer';

const HomeScreen = () => {
    return (
        <>
            <TopbarMain />
            <div className="homeScreen">
                <section id="/"><Home /></section>
                <section id="about"><About /></section>
                <section id="experience"><Experience /></section>
                <section id="works"><Work /></section>
                <section id="contact"><Contact /></section>
                <Footer />
            </div>

        </>
    );
};

export default HomeScreen;