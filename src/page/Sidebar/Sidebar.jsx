import React, { useEffect, useRef, useState } from 'react';
import './_sideBar.scss';
import HomeIcon from '@material-ui/icons/Home';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import SchoolIcon from '@material-ui/icons/School';
import BurstModeIcon from '@material-ui/icons/BurstMode';
import SmsIcon from '@material-ui/icons/Sms';
import LockIcon from '@material-ui/icons/Lock';
import { useDispatch, useSelector } from 'react-redux';
import { toogleSidebar } from '../../redux/actions/sidbar.actions';
import ClearIcon from '@material-ui/icons/Clear';

const Sidebar = () => {

    const date = new Date();
    const year = date.getFullYear();
    const dispatch = useDispatch();

    const accessToken = useSelector(state => state.sideBarToggle.sidebarToggle);
    const hideSidebar = () => {
        dispatch(toogleSidebar());
    };

    const index = useRef(0);
    const text = `© ${year} Afzal-Bidhan.`;
    const [currentText, setCurrentText] = useState("");

    useEffect(() => {
        setTimeout(() => {
            setCurrentText((value) => value + text.charAt(index.current));
            index.current += 1;
        }, 150)
    }, [currentText, text]);

    return (
        <div>
            <div className={accessToken ? "sidebar open" : "sidebar"} onClick={() => hideSidebar(false)}>
                <div className="sidebar_logo d-flex">
                    <h1 className="title_font">Afzal</h1>
                    <span className="dot"></span>
                    <h1><span className="clear"><ClearIcon /></span></h1>
                </div>
                <nav>
                    <ul>
                        <li><HomeIcon /><a href="#/">Home</a></li>
                        <li><PermIdentityIcon /><a href="#about">About</a></li>
                        <li><SchoolIcon /><a href="#experience">Experience</a></li>
                        <li><BurstModeIcon /><a href="#works">Works</a></li>
                        <li><SmsIcon /><a href="#contact">Contact</a></li>
                        <li><LockIcon /><a href="#!">Admin</a></li>
                    </ul>
                </nav>

                <div className="sidebar_footer">
                    <p>{currentText}</p>
                </div>
            </div>
        </div>
    );
};

export default Sidebar;