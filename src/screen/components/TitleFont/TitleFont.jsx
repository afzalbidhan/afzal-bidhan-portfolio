import React from 'react'

const TitleFont = ({ text, anim }) => {

    const style = {
        workCarouselTitle: {
            fontFamily: "'Kaushan Script', cursive",
            color: "black",
            textAlign: "center",
            marginTop: "2rem",
            marginBottom: "2rem",
        }
    };

    return (
        <div
            style={style.workCarouselTitle}
            className="workCarousel_title"
            data-aos-duration="1400"
            data-aos={anim}
            data-aos-once="true"
        >
            <h1>{text}</h1>
        </div>
    )
}

export default TitleFont
