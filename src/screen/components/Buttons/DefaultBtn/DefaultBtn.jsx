import React from 'react';
import './_defaultBtn.scss';
import Button from '@material-ui/core/Button';

const DefaultBtn = ({ path, download, color, text, classs, ...other }) => {
    return (
        <div className="defaultBtn">
            <Button
                className={classs}
                variant="contained"
                color={color}
                download={download}
                href={path}
                {...other}>
                {text}
            </Button>
        </div>
    );
};

export default DefaultBtn;