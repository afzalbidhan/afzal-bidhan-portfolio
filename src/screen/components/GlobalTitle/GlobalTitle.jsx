import React from 'react'
import './_globalTitle.scss';

const GlobalTitle = ({ text, anim }) => {
    return (
        <div
            data-aos-duration="1400"
            data-aos={anim}
            data-aos-once="true"
            className="globalTitle">
            <h1>{text}</h1>
        </div>
    )
}

export default GlobalTitle
