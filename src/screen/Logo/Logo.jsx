import React from 'react';
import './_logo.scss';

const Logo = () => {
    return (
        <div className="logo d-flex">
            <h1 className="title_font">Afzal</h1>
            <span className="dot"></span>
        </div>
    )
}

export default Logo
