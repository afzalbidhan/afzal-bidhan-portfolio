import React from 'react';
import './_timeline.scss';
import EventIcon from '@material-ui/icons/Event';

const Timeline = ({ date, title, description }) => {
    return (
        <div className="timeline-item">
            <div
                className="circle-dot"
                data-aos-duration="1000"
                data-aos="fade-up"></div>
            <div
                data-aos-duration="1400"
                data-aos="fade-up">
                <h6 className="timeline-data">
                    <EventIcon /> <span>{date}</span>
                </h6>
                <h4 className="timeline-title">{title}</h4>
                <p className="timeline-text">{description}</p>
            </div>
        </div>
    );
};

export default Timeline;