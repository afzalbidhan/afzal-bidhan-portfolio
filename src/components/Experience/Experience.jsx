import React from 'react';
import './_experience.scss';
import timelineTtemData from '../../Data/timelineTtemData';
import Timeline from './components/Timeline';
import GlobalTitle from './../../screen/components/GlobalTitle/GlobalTitle';

const Experience = () => {
    return (
        <div className="experience">
            <div className="container">
                <GlobalTitle text="Experience" anim="fade-right" />
                <div className="row about-Info">
                    <div className="col-md-6">
                        <div className="timeline">
                            {timelineTtemData.slice(0, 2).map(data =>
                                <Timeline
                                    key={data.id}
                                    date={data.date}
                                    description={data.description}
                                    title={data.title}
                                />
                            )}
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="timeline">
                            {timelineTtemData.slice(2, 4).map(data =>
                                <Timeline
                                    key={data.id}
                                    date={data.date}
                                    description={data.description}
                                    title={data.title}
                                />
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Experience;