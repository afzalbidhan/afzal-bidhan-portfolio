import React from 'react';
import './_homeParallax.scss';
import { ParallaxData } from '../../Data/parallax';

// home parallax effect function
export const parallaxEffect = (e) => {
    document.querySelectorAll('.layer').forEach(layer => {
        const speed = layer.getAttribute('dataspeed');
        const x = (window.innerWidth - e.pageX * speed) / 100;
        const y = (window.innerHeight - e.pageY * speed) / 100;
        layer.style.transform = `translate(${x}px, ${y}px)`;
    });
};

const HomeParallax = () => {

    return (
        <div className="homeParallax" onMouseMove={(e) => parallaxEffect(e)}>
            {
                ParallaxData.map(data => {
                    const { dataSpeed, width, height, className, path, id } = data;
                    return (
                        <svg
                            className={className}
                            dataspeed={dataSpeed}
                            width={width}
                            height={height}
                            xmlns="http://www.w3.org/2000/svg"
                            key={id}
                        >
                            {path}
                        </svg>
                    )
                })
            }
        </div>
    );
};

export default HomeParallax;