import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getWorks, workPageAction } from '../../redux/actions/works.actions';
import { filterWorksAction } from '../../redux/actions/filterWork.action';
import WorkPage from './components/WorkPagination/WorkPage';
import WorkFilterBtn from './components/WorkFilterBtn/WorkFilterBtn';
import WorkData from './components/WorkData/WorkData';
import LatestWork from './components/LatestWork/LatestWork';
import WorkInfo from './components/WorkInfo/WorkInfo';

const Work = () => {
    const worksRef = useRef();
    const dispatch = useDispatch();
    const works = useSelector((state) => state.works);
    const getFilterWorks = useSelector((state) => state.filterWorksReducer);
    const { worksData, loading, pageNumber } = works;
    const { filterWorksData } = getFilterWorks
    const usersPerPage = 6;
    const pagesVisited = pageNumber * usersPerPage;
    const workspageCount = Math.ceil(worksData.length / usersPerPage);
    const filterWorkspageCount = Math.ceil(filterWorksData.length / usersPerPage);

    useEffect(() => {
        dispatch(getWorks());
    }, [dispatch]);

    const worksDataFilterFn = (value) => {
        dispatch(filterWorksAction(worksData, value));
        dispatch(workPageAction(0))
    };
    const allWorksData = (data) => {
        dispatch(filterWorksAction(data));
        dispatch(workPageAction(0))
    };
    const filterOnPageChange = ({ selected }) => {
        dispatch(workPageAction(selected));
    };

    return (
        <div className="works my-5">
            <LatestWork />
            <div ref={worksRef}>
                <WorkFilterBtn
                    allWorksData={allWorksData}
                    worksDataFilterFn={worksDataFilterFn}
                    worksData={worksData}
                />
                <WorkData
                    loading={loading}
                    filterWorksData={filterWorksData}
                    pagesVisited={pagesVisited}
                    usersPerPage={usersPerPage}
                    worksData={worksData}
                />
            </div>
            <WorkPage
                filterWorksData={filterWorksData}
                filterWorkspageCount={filterWorkspageCount}
                pageNumber={pageNumber}
                workspageCount={workspageCount}
                filterOnPageChange={filterOnPageChange}
                worksRef={worksRef}
            />
            <WorkInfo />
        </div>
    );
};

export default Work;