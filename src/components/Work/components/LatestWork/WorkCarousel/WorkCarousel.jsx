import React, { useEffect } from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { useDispatch, useSelector } from 'react-redux';
import { worksCarouselAction } from '../../../../../redux/actions/worksCarousel.actions';

const WorkCarousel = () => {

    const dispatch = useDispatch();
    useSelector((state) => state.works.loading);
    const works = useSelector((state) => state.worksCarouselReducers);
    const { worksCarouselData, CarouselDataLoading } = works;
    const slicewWorksData = worksCarouselData.reverse().slice(0, 4);
    useEffect(() => {
        dispatch(worksCarouselAction());
    }, [dispatch]);

    return (
        <OwlCarousel
            className='owl-theme'
            loop={true}
            margin={0}
            nav={true}
            items={1}
            // autoplay={true}
            smartSpeed={1200}
        >
            {CarouselDataLoading ? slicewWorksData.map(item => (
                <div class='item' key={item._id}>
                    <img className="img-fluid" src={item.image} alt="" />
                </div>
            )) :
                <svg style={{ margin: "auto", background: "#fff", display: "block" }} width="200px" height="200px" viewBox="0 0 100 100">
                    <path fill="none" stroke="#e90c59" strokeWidth="8" strokeDasharray="42.76482137044271 42.76482137044271" d="M24.3 30C11.4 30 5 43.3 5 50s6.4 20 19.3 20c19.3 0 32.1-40 51.4-40 C88.6 30 95 43.3 95 50s-6.4 20-19.3 20C56.4 70 43.6 30 24.3 30z" strokeLinecap="round" style={{ transform: "scale(0.8)", transformOrigin: "50px 50px" }}><animate attributeName="stroke-dashoffset" repeatCount="indefinite" dur="1s" keyTimes="0;1" values="0;256.58892822265625"></animate>
                    </path>
                </svg>

            }
        </OwlCarousel>
    )
}

export default WorkCarousel
