import React from 'react';
import './_latestWork.scss';
import WorkCarousel from './WorkCarousel/WorkCarousel';
import img from '../../../../images/img/testimonial.bc1380d8.svg'
import GlobalTitle from '../../../../screen/components/GlobalTitle/GlobalTitle';


const LatestWork = () => {

    return (
        <div className="latestWorkCarousel">
            <div className="container mb-5">
                <GlobalTitle text="Works." anim="flip-right" />
                <div className="row">
                    <div className="col-md-6">
                        <div className="latest_work_img">
                            <img
                                className="img-fluid"
                                data-aos-duration="1500"
                                data-aos="zoom-in-up"
                                data-aos-once="true"
                                src={img}
                                alt="" />
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="latest_work">
                            <WorkCarousel />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LatestWork;