import React from 'react';
import './_workItem.scss';
import RemoveRedEyeIcon from '@material-ui/icons/RemoveRedEye';
import LiveTvIcon from '@material-ui/icons/LiveTv';
import DescriptionIcon from '@material-ui/icons/Description';

const WorkItem = ({ elem }) => {

    const { name, image, category, type, update } = elem;

    return (
        <div className="col-xl-4 col-lg-6 col-md-6 mt-5">
            <div
                data-aos-duration="1500"
                data-aos="fade-up"
                className="card">
                <img src={image} className="card__image img-fluid" alt="" />
                <div className="card__overlay">
                    <div className="card__header">
                        <a href="#!"><RemoveRedEyeIcon className="live-icon" /></a>
                        <div className="card__header-text">
                            <h3 className="card__title">{name}</h3>
                            <span className="card__status">{category} {type}</span>
                            {update ? <div className="lable_update"><button>{update}...</button></div> : ""}
                        </div>
                    </div>
                    <p className="card__description">
                        <a href="#!"><button className="btn1">Preview <LiveTvIcon /></button></a>
                        <a href="#!"><button className="btn2">Details <DescriptionIcon /></button></a>
                    </p>
                </div>
            </div>


        </div>
    );
};

export default WorkItem;