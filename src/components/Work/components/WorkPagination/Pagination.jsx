import React from 'react';
import ReactPaginate from 'react-paginate';
import { useDispatch } from 'react-redux';
import { workPageAction } from '../../../../redux/actions/works.actions';
import './_pagination.scss';


const Pagination = ({ pageCount, forcePage, worksRef }) => {
    const worksTop = worksRef.current.offsetTop;
    const dispatch = useDispatch();
    const pageChange = ({ selected }) => {
        dispatch(workPageAction(selected));
        window.scrollTo({ top: worksTop, behavior: "smooth" })
    };

    return (
        <ReactPaginate
            previousLabel={"Prev"}
            nextLabel={"Next"}
            pageCount={pageCount}
            forcePage={forcePage}
            onPageChange={pageChange}
            containerClassName={"paginationBttns"}
            previousLinkClassName={"previousBttns"}
            nextLinkClassName={"nextBttn"}
            disabledClassName={"paginationDisabled"}
            activeClassName={"paginationActive"}
        />
    );
};

export default Pagination;