import React from 'react';
import Pagination from './Pagination';

const WorkPage = ({
    filterWorksData,
    filterWorkspageCount,
    pageNumber,
    workspageCount,
    filterOnPageChange,
    worksRef
}) => {
    return (
        <div>
            {
                filterWorksData.length ? filterWorkspageCount > 1 ?
                    <Pagination
                        pageCount={filterWorkspageCount}
                        forcePage={pageNumber}
                        onPageChange={filterOnPageChange}
                        worksRef={worksRef}
                    /> : ""

                    : workspageCount > 1 ?
                        <Pagination
                            forcePage={pageNumber}
                            pageCount={workspageCount}
                            onPageChange={filterOnPageChange}
                            worksRef={worksRef}
                        /> : ""
            }
        </div>
    );
};

export default WorkPage;