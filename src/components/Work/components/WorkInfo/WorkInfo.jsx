import React from 'react'
import './_workInfo.scss';
import img from '../../../../images/img/service.67717712 (1).svg';
import { listItem1st, listItem2nd } from '../../../../Data/workList';

const WorkInfo = () => {
    return (
        <div className="workInfo">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="work_info_item mb-5">
                            <p className="first_item">WHAT I DO ?</p>
                            <h1>Innovative Solutions To <br /> Grow Your Creative <br /> Projects</h1>
                            <hr />
                            <p>I’m Bidhan, a visual designer based in BD. I specialize in <br /> helping startups grow their businesses.</p>
                            <hr />
                            <ul className="list_1">
                                {listItem1st.map(item => (
                                        <li
                                            data-aos-duration="1500"
                                            data-aos="fade-up"
                                            key={item.id}>
                                            <span className="text-success">✔ </span>
                                            {item.text}
                                        </li>
                                    ))}
                            </ul>
                            <hr />
                            <ul className="list_2">
                                {listItem2nd.map(item => (
                                        <li
                                            data-aos-duration="1500"
                                            data-aos="fade-up"
                                            key={item.id}>
                                            <span className="text-success">✔ </span>
                                            {item.text}
                                        </li>
                                    ))}
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-6 img">
                        <img
                            data-aos-delay="1000"
                            data-aos-duration="1500"
                            data-aos="zoom-in-up"
                            data-aos-once="true"
                            className="img-fluid" src={img} alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default WorkInfo
