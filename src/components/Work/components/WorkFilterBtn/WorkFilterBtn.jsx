import React from 'react';
import { useSelector } from 'react-redux';
import './_workFilterBtn.scss'

const WorkFilterBtn = ({ allWorksData, worksDataFilterFn, worksData }) => {

    const filteringWork = useSelector((state) => state.filterWorksReducer);
    const { filterWorkName, filterWorksData } = filteringWork;
    const filterLeng = filterWorksData.length;
    const workLeng = worksData.length;
    const workName = filterWorkName;

    return (
        <div className="workFilterBtn_main container">
            <div className="workFilterBtn">
                <button onClick={() => allWorksData(worksData)}>All</button>
                <button onClick={() => worksDataFilterFn("React")}>React</button>
                <button onClick={() => worksDataFilterFn("Javascript")}>Javascript</button>
                <button onClick={() => worksDataFilterFn("Html-Css")}>Html-Css</button>
                <p
                    data-aos-duration="1000"
                    data-aos="flip-left"
                    data-aos-once="true"
                    className="filter_count">
                    <strong className="text-success">
                        {
                            filterLeng === 0 ? "All" : workName
                        } - {
                            filterLeng === 0 ? workLeng : filterLeng
                        }
                    </strong>
                    <small><b> works</b></small>
                </p>
            </div>
        </div>
    );
};

export default WorkFilterBtn;