import React from 'react';
import './_footer.scss';

const Footer = () => {

    const date = new Date();
    const year = date.getFullYear();

    return (
        <footer class="footer-section">
            <div class="container">
                <div class="footer-cta pt-5">
                    <div class="row">
                        <div class="col-xl-4 col-md-4 mb-30">
                            <div class="single-cta">
                                <i class="fas fa-map-marker-alt"></i>
                                <div class="cta-text">
                                    <h4>Find me</h4>
                                    <span>Shibchar, Madaripur, Bangladesh</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 mb-30">
                            <div class="single-cta">
                                <i class="fas fa-phone"></i>
                                <div class="cta-text">
                                    <h4>Call me</h4>
                                    <span>+880 1793926521</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4 mb-30">
                            <div class="single-cta">
                                <i class="far fa-envelope-open"></i>
                                <div class="cta-text">
                                    <h4>Mail me</h4>
                                    <span>afzalbidhan@gmail.com</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-content pt-5 pb-5">
                    <div class="row">

                        <div class="col-xl-8 col-lg-8 col-md-6 mb-50">
                            <div class="footer-widget">
                                <div className="logo d-flex">
                                    <h1 className="title_font">Afzal</h1>
                                    <span className="dot"></span>
                                </div>
                                <div class="footer-text">
                                    <p>I said a lot about the present. Surely I have worked hard in the past so I am at a good stage now. In the same way, if I keep my present busy, I think my future will be bright. So I try to get my work done through hard work. I don't know anything yet I have low talent but I try I don't give up I have seen success every time I tried. If I can't do anything, at least I'll try. Now though I am working on Front-End Development. Then my next target will be Full-Stack Development. Then I will try to learn something new to keep pace with the times.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                            <div class="footer-social-icon">
                                <span>Follow us</span>
                                <a href="#!"><i class="fab fa-facebook-f facebook-bg"></i></a>
                                <a href="#!"><i class="fab fa-twitter twitter-bg"></i></a>
                                <a href="#!"><i class="fab fa-google-plus-g google-bg"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-area">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 text-center text-lg-left">
                            <div class="copyright-text">
                                <p>Copyright &copy; {year}, All Right Reserved <a href="#!">Afzal-Bidhan</a></p>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                            <div class="footer-menu">
                                <ul>
                                    <li><a href="#/">Home</a></li>
                                    <li><a href="#!">Terms</a></li>
                                    <li><a href="#!">Privacy</a></li>
                                    <li><a href="#!">Policy</a></li>
                                    <li><a href="#contact">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer
