import React, { useEffect, useState } from 'react';
import { useWindowScroll } from 'react-use';
import './_scrollToTopIcon.scss';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';

const ScrollToTopIcon = () => {

    const { y: pageYOffset } = useWindowScroll();
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        if (pageYOffset > 200) {
            setVisible(true);
        } else {
            setVisible(false);
        }
    }, [pageYOffset]);

    const scrollToTop = () => window.scrollTo({ top: 0, behavior: "smooth" })

    if (!visible) {
        return false;
    }

    return <ArrowUpwardIcon onClick={scrollToTop} className="scroll_icon"/>
};

export default ScrollToTopIcon;