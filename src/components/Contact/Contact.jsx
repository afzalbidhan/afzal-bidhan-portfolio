import React, { useState } from 'react';
import './_contact.scss';
import ContactSvg from './ContantSvg';
import emailjs from 'emailjs-com';
import GlobalTitle from './../../screen/components/GlobalTitle/GlobalTitle';

const Contact = () => {

    const [emailSuccess, setEmailSuccess] = useState({
        success: "",
    });

    function sendEmail(e) {
        e.preventDefault();
        emailjs.sendForm('service_qof1njh', 'template_jdrcb3l', e.target, 'user_mtQcgA3CaILkDkwHRtUYP')
            .then((result) => {
                if (result.status === 200) {
                    const emailSend = { ...emailSuccess };
                    emailSend.success = "Your message send successful"
                    setEmailSuccess(emailSend);
                }
            }, (error) => {
                alert("Your message send Fail")
            });
        e.target.reset();
    };

    return (
        <div className="contact mb-5" >
            <div class="container">
                <GlobalTitle text="Contact" anim="zoom-in" />
                <div className="row">
                    <div className="col-lg-6">
                        <ContactSvg />
                    </div>
                    <div className="col-lg-6">
                        <h1 class="title text-center mb-4">Talk to Us</h1>
                        <h6 className="text-center send_success">{emailSuccess.success}</h6>
                        <form
                            data-aos-duration="1500"
                            data-aos="fade-up"
                            onSubmit={(e) => sendEmail(e)}>
                            <div class="form-group position-relative">
                                <input type="text" name="name" class="form-control form-control-lg thick" placeholder="Full name" required />
                            </div>
                            <div class="form-group position-relative">
                                <input type="email" name="email" class="form-control form-control-lg thick" placeholder="Email" required />
                            </div>
                            <div class="form-group position-relative">
                                <input type="tel" name="phone" class="form-control form-control-lg thick" placeholder="Phone" />
                            </div>
                            <div class="form-group message">
                                <textarea name="message" class="form-control form-control-lg" rows="7" placeholder="Message"></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary" tabIndex="-1">Send message</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Contact
