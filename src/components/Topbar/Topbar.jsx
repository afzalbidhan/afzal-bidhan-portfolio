import React from 'react';
import './_topbar.scss';
import AppsIcon from '@material-ui/icons/Apps';
import { useDispatch } from 'react-redux';
import { toogleSidebar } from '../../redux/actions/sidbar.actions';


const Topbar = () => {

    const dispatch = useDispatch();
    const showSidebar = () => {
        dispatch(toogleSidebar());
    };


    return (
        <div className="topbar">
            <div className="topbar_items">
                <AppsIcon onClick={() => showSidebar()} />
                <div className="sidebar_logo d-flex">
                    <h1 className="title_font">Afzal</h1>
                    <span className="topbar_dot"></span>
                </div>
            </div>
        </div>
    );
};

export default Topbar;