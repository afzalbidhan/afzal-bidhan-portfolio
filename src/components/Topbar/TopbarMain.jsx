import React, { Component } from 'react';
import Topbar from './Topbar';

class TopbarMain extends Component {

    constructor() {
        super();
        this.state = {
            show: true,
            scrollPos: 0,
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll)
    };
    componentWillUnmount() {
        window.addEventListener("scroll", this.handleScroll)
    };

    handleScroll = () => {
        this.setState({
            scrollPos: document.body.getBoundingClientRect().top,
            show: document.body.getBoundingClientRect().top > this.state.scrollPos,
        });
    };

    render() {
        return (
            <div className="topbarMain">
                <div className={this.state.show ? "topbar_active" : "topbar_hidden"}>
                <Topbar />
            </div>
            </div>
        );
    }
}

export default TopbarMain;