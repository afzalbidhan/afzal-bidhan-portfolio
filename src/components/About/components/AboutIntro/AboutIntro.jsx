import React from 'react'
import DefaultBtn from './../../../../screen/components/Buttons/DefaultBtn/DefaultBtn';
import AboutCarousel from './../AboutCarousel/AboutCarousel';
import { about_intro_text } from './../../../../Data/dataAll';
import resume from '../../../../document/resume.pdf';
import './_aboutIntro.scss';

const AboutIntro = () => {
    return (
        <div className="col-md-9">
            <div
                className="sort_about"
                data-aos-duration="1400"
                data-aos="zoom-in-left"
                data-aos-once="true">
                <div className="row">
                    <div className="col-md-6">
                        <h3 className="about_intro">
                            Intro<span>...</span>
                        </h3>
                        <p className="about_intro_text">{about_intro_text}</p>
                        <DefaultBtn
                            text="Download CV"
                            color="secondary"
                            classs="about_btn"
                            path={resume}
                            download="afzal-bidhan-resume"
                        />
                    </div>
                    <div className="col-md-6">
                        <AboutCarousel />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AboutIntro
