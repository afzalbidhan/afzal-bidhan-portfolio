import React, { useRef } from "react";
import "./_aboutSkillBar.scss";
import aboutSkillBar from './../../../../Data/aboutSkillBar';
import SkillBar from './components/SkillBar/SkillBar';

const AboutSkillBar = () => {

  const inputRef = useRef();
  const firstData = aboutSkillBar.slice(0, 7);
  const secondData = aboutSkillBar.slice(7, 14);

  return (
    <div ref={inputRef} className="col-lg-6 col-md-12">
      <div className="skillsSection">
        <div className="row">
          <div className="col-md-6">
            {
              firstData.map(item => (
                <SkillBar
                  key={item.id}
                  inputRef={inputRef}
                  name={item.name}
                  img={item.img}
                  basic={item.basic}
                  duration={item.duration}
                  dataProgress={item.dataProgress}
                  color={item.color}
                />
              ))
            }
          </div>
          <div className="col-md-6">
            {
              secondData.map(item => (
                <SkillBar
                  key={item.id}
                  inputRef={inputRef}
                  name={item.name}
                  img={item.img}
                  basic={item.basic}
                  dataProgress={item.dataProgress}
                  color={item.color}
                />
              ))
            }
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutSkillBar;
