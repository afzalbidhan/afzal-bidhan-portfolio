import React, { useEffect } from 'react'
import useSkillbar from './../useSkillbar';
import './_skillBar.scss';

const SkillBar = ({ name, img, basic, dataProgress, duration, color, inputRef }) => {

    const { CountUp, progressBarLine } = useSkillbar(inputRef);
    useEffect(() => {
        progressBarLine()
    }, [progressBarLine]);

    return (
        <div className="skill-item">
            <h5>
                {name}
                <img src={img} alt="" /><small>{basic}</small>
                <span>{<CountUp number={dataProgress} duration={duration} />}%</span>
            </h5>
            <div className="progress">
                <div
                    className="progress-bar"
                    style={{ backgroundColor: `${color}` }}
                    data-progress={dataProgress}
                ></div>
            </div>
        </div>
    )
}

export default SkillBar
