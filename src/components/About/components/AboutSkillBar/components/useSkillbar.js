import { useEffect, useState } from 'react'

const useSkillbar = (inputRef) => {

    const easeOutQuad = (t) => t * (2 - t);
    const frameDuration = 1000 / 60;
    const CountUp = ({ number, duration = 2200 }) => {
        const countTo = parseInt(number, 10);
        const [count, setCount] = useState(0);
        useEffect(() => {
            let a = 0;
            window.addEventListener("scroll", () => {
                const sectionPos = inputRef.current.getBoundingClientRect().top - window.innerHeight / 1.5;
                if (a === 0 && window.scrollX > sectionPos) {
                    let frame = 0;
                    const totalFrames = Math.round(duration / frameDuration);
                    const counter = setInterval(() => {
                        frame++;
                        const progress = easeOutQuad(frame / totalFrames);
                        setCount(countTo * progress);

                        if (frame === totalFrames) {
                            clearInterval(counter);
                        }
                    }, frameDuration);
                    a = 1;
                }
            });
        }, [countTo, duration]);
        return Math.floor(count);
    };

    const progressBarLine = () => {
        window.addEventListener("scroll", () => {
            const sectionPos = inputRef.current.getBoundingClientRect().top;
            const screenPos = window.innerHeight / 1.4;
            if (sectionPos < screenPos) {
                document.querySelectorAll(".progress-bar").forEach((progressBars) => {
                    const value = progressBars.dataset.progress;
                    progressBars.style.opacity = 1;
                    progressBars.style.width = `${value}%`;
                });
            }
        });
    };


    return {
        CountUp,
        progressBarLine
    }
}

export default useSkillbar
