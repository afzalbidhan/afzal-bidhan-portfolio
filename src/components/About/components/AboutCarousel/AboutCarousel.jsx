import React from 'react';
import './_sboutCarousel.scss';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import aboutCarousel from './../../../../Data/aboutCarousel';

const AboutCarousel = () => {
    return (
        <OwlCarousel
            className='owl-theme'
            loop={true}
            margin={0}
            nav={true}
            items={1}
            autoplay={true}
            smartSpeed={1200}
        >
            {
                aboutCarousel.map(item => (
                    <div className='item'>
                        <img className="img-fluid" src={item.img} alt="" />
                    </div>
                ))
            }
        </OwlCarousel>
    );
};

export default AboutCarousel;