import React from 'react'
import './_aboutInfo.scss';
import aboutInfo from './../../../../Data/aboutInfo';

const AboutInfo = () => {
    return (
        <div className="col-lg-6 col-md-12">
            <div className="aboutInfo">
                <h3>Beautiful & consistant UI <br /> powered with <span>React</span>.</h3>
                <div className="row">
                    {
                        aboutInfo.map(item => (
                            <div
                                className="col-md-6 col-sm-6 col-6"
                                key={item.id}
                                data-aos-duration="1500"
                                data-aos="fade-up"
                            >
                                <img src={item.img} alt={item.name} />
                                <h6>{item.name}</h6>
                                <p>{item.description1}<br />{item.description2}</p>
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>

    )
}

export default AboutInfo
