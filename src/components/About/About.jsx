import React from 'react';
import './_about.scss';
import AboutSkillBar from './components/AboutSkillBar/AboutSkillBar';
import "aos/dist/aos.css";
import profile from '../../images/bidhan_img/afzal-profile.jpg';
import AboutIntro from './components/AboutIntro/AboutIntro';
import GlobalTitle from '../../screen/components/GlobalTitle/GlobalTitle';
import AboutInfo from './components/AboutInfo/AboutInfo';
import TitleFont from './../../screen/components/TitleFont/TitleFont';

const About = () => {

    return (
        <div className="about">
            <div className="container">
                <GlobalTitle text="About Me" anim="fade-up" />
                <div className="row">
                    <div className="col-md-3">
                        <div className="about_profile">
                            <img className="img-fluid" src={profile} alt="" />
                        </div>
                    </div>
                    <AboutIntro />
                </div>
                <TitleFont text="Skills & info." anim="fade" />
                <div className="row">
                    <AboutInfo />
                    <AboutSkillBar />
                </div>
            </div>
        </div>
    );
};

export default About;