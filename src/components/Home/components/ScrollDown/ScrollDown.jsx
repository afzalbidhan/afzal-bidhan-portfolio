import React from 'react';
import './_scrollDown.scss';

const ScrollDown = () => {
    return (
        <div className="scrollDown_main">
            <div class='scrolldown'>
                <div class="chevrons">
                    <div class='chevrondown'></div>
                    <div class='chevrondown'></div>
                </div>
            </div>
        </div>
    );
};

export default ScrollDown;