import React from 'react';
import './_titleAnim.scss';

const TitleAnim = () => {
    return (
        <div className="title_rotate">
            <span>A</span>
            <span>f</span>
            <span>z</span>
            <span>a</span>
            <span>l</span>
            <span className="dit_hide">.</span>
            <span>B</span>
            <span>i</span>
            <span>d</span>
            <span>h</span>
            <span>a</span>
            <span>n</span>
        </div>
    );
};

export default TitleAnim;