import React from 'react';
import './_homeSociaLinks.scss'

const HomeLinks = ({ href, icon }) => (
    <div
        className="icon-item"
        data-aos="zoom-in"
        data-aos-duration="1000"
        data-aos-delay="400">
        <a href={href} target="blank" className="icon-link">
            <i className={`fab fa-${icon}`}></i>
        </a>
    </div>
);

const HomeSociaLinks = ({ facebook, instagram, linkdin, github, twitter }) => {
    return (
        <div className="icon-list">
            <HomeLinks href={facebook} icon={"facebook-f"} classs={"fb"} />
            <HomeLinks href={linkdin} icon={"linkedin-in"} />
            <HomeLinks href={github} icon={"github"} />
            <HomeLinks href={instagram} icon={"instagram"} />
            <HomeLinks href={twitter} icon={"twitter"} />
        </div>
    );
};

export default HomeSociaLinks;