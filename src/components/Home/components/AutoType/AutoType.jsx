import React from 'react';
import './_autoType.scss';
import Typewriter from 'typewriter-effect';

const AutoType = () => {
    return (
        <div className="dynamic_text d-flex">
            <span>I am a</span>
            <Typewriter
                options={{
                    autoStart: true,
                    loop: true,
                    delay: 40,
                    strings: [
                        "React Developer",
                        "Front-End Developer",
                        "Responsive Designer"
                    ]
                }}
            />
        </div>
    );
};

export default AutoType;