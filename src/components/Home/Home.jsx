import React, { useEffect } from 'react';
import HomeSociaLinks from './components/HomeSociaLinks/HomeSociaLinks';
import HomeParallax, { parallaxEffect } from '../HomeParallax/HomeParallax';
import './_home.scss';
import profile from '../../images/bidhan_img/afzal-profile.jpg';
import AutoType from './components/AutoType/AutoType';
import TitleAnim from './components/TitleAnim/TitleAnim';
import ScrollDown from './components/ScrollDown/ScrollDown';
import { useDispatch, useSelector } from 'react-redux';
import { facebookLink, githubLink, instagramLink, linkdinLink, twitterLink } from '../../redux/actions/socialLinks.actions';
import DefaultBtn from './../../screen/components/Buttons/DefaultBtn/DefaultBtn';
import Aos from 'aos';

const Home = () => {

    const dispatch = useDispatch();
    const links = useSelector((state) => state.links);
    const { facebook, github, instagram, linkdin, twitter } = links

    useEffect(() => {
        dispatch(twitterLink());
        dispatch(facebookLink());
        dispatch(instagramLink());
        dispatch(linkdinLink());
        dispatch(githubLink());
        Aos.init();
    }, [dispatch]);


    return (
        <div
            className="home d-flex align-items-center"
            onMouseMove={(e) => parallaxEffect(e)}>
            <div className='container'>
                <div className="home_items">
                    <img
                        data-aos="zoom-in"
                        data-aos-duration="800"
                        src={profile}
                        alt=""
                    />
                    <TitleAnim />
                    <AutoType />
                    <HomeSociaLinks
                        facebook={facebook}
                        github={github}
                        instagram={instagram}
                        linkdin={linkdin}
                        twitter={twitter}
                    />
                    <DefaultBtn
                        color="secondary"
                        text="Hire Me"
                        classs="home_Btn"
                    />
                    <ScrollDown />
                </div>
            </div>
            <div className="parallax_hide">
                <HomeParallax />
            </div>
        </div>
    );
};

export default Home;