import React from 'react';
import './_app.scss';
import HomeScreen from '../page/HomeScreen/HomeScreen';
import Sidebar from '../page/Sidebar/Sidebar';
import ScrollToTopIcon from '../components/ScrollPage/ScrollToTopIcon';

function App() {

  return (
    <>
      <Sidebar />
      <HomeScreen />
      <ScrollToTopIcon />
    </>
  );
}

export default App;